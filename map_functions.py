import psycopg2
import pandas.io.sql as sqlio
 
conn = psycopg2.connect(host='localhost', dbname='nyc_routing', user='postgres', password='Ieatqas7535#')

def calc_k_shortest_paths(source, target, num_paths):
	sql = ("DROP TABLE IF EXISTS k_paths; DROP TABLE IF EXISTS k_paths2;" + 
		"CREATE TABLE k_paths AS SELECT seq, id1 AS route, id2 AS node, id3 AS edge, cost FROM pgr_ksp('SELECT gid as id, source::int4, target::int4, cost_s::float8 AS cost, reverse_cost_s::float8 AS reverse_cost FROM ways', %(source)s, %(target)s, %(num_paths)s, True);" +
	"CREATE TABLE k_paths2 AS SELECT route, node, lon, lat, edge FROM ways_vertices_pgr, k_paths where ways_vertices_pgr.id = k_paths.node;" + 
	"SELECT route, node, lon, lat, edge, the_geom FROM k_paths2, ways WHERE ways.gid=k_paths2.edge;")
	
	df = sqlio.read_sql(sql % {'source':source, 'target': target, 'num_paths': num_paths}, conn)

	return df